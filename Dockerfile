FROM node:7.2.1

RUN curl -o- -L https://yarnpkg.com/install.sh | bash && \
    mkdir -p /usr/src/app

WORKDIR /usr/src/app
